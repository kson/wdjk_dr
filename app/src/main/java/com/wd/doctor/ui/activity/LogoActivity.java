package com.wd.doctor.ui.activity;

import android.os.Handler;
import android.os.Message;

import com.wd.doctor.R;
import com.wd.lib_core.base.BaseActivity;

public class LogoActivity extends BaseActivity {

    private int i = 0;

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(com.wd.lib_core.R.anim.slide_in_scale, com.wd.lib_core.R.anim.slide_out_scale);
    }

    @Override
    protected int bindLayout() {
        return R.layout.activity_logo;
    }

    protected void initView() {
        setImmsionBar();
    }

    @Override
    protected void initData() {
        handler.sendEmptyMessageDelayed(0, 1000);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                i++;
                if (i > 2) {
                    //ARouter.getInstance().build("/module/acc").navigation();
                    //startIntent(LoginActivity.class);
                    overridePendingTransition(com.wd.lib_core.R.anim.slide_in_alpha, com.wd.lib_core.R.anim.slide_out_alpha);
                    finish();
                } else {
                    handler.sendEmptyMessageDelayed(0, 1000);
                }
            }
        }
    };
}

