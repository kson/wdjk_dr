package com.wd.lib_net.api;

/**
 * Author:杨帅
 * Date:2019/8/6 9:20
 * Description：Api接口类
 */
public class Api {
    public static final String BASE_URL = Constant.CONTANT_TYPE ? "http://172.17.8.100/health/doctor/" : "http://mobile.bwstudent.com/health/doctor/";
    // Doctor相关
    public static final String SENDEMAILCODE_URL = "v1/sendEmailCode";
    public static final String APPLYJOIN_URL = "v1/applyJoin";
    public static final String LOGIN_URL = "v1/login";
    public static final String FINDDOCTORBYID_URL = "verify/v1/findDoctorById";
    public static final String UPLOADIMAGEPIC_URL = "verify/v1/uploadImagePic";
    public static final String CHOOSEIMAGEPIC_URL = "verify/v1/chooseImagePic";
    public static final String FINDSYSTEMIMAGEPIC_URL = "v1/findSystemImagePic";
    public static final String BINDDOCTORBANKCARD_URL = "verify/v1/bindDoctorBankCard";
    public static final String FINDDOCTORBANKCARDBYID_URL = "verify/v1/findDoctorBankCardById";
    public static final String BINDDOCTORIDCARD_URL = "verify/v1/bindDoctorIdCard";
    public static final String FINDDOCTORIDCARDINFO_URL = "verify/v1/findDoctorIdCardInfo";
    public static final String FINDDOCTORDRAWRECORDLIST_URL = "verify/v1/findDoctorDrawRecordList";
    public static final String FINDDOCTORINCOMERECORDLIST_URL = "verify/v1/findDoctorIncomeRecordList";
    public static final String FINDDOCTORWALLET_URL = "verify/v1/findDoctorWallet";
    public static final String DRAWCASH_URL = "verify/v1/drawCash";
    public static final String ADDDOCTORPUSHTOKEN_URL = "verify/v1/addDoctorPushToken";
    public static final String FINDDOCTORGIFTLIST_URL = "verify/v1/findDoctorGiftList";
    public static final String FINDDOCTORSYSTEMNOTICELIST_URL = "verify/v1/findDoctorSystemNoticeList";
    public static final String FINDDOCTORINQUIRYNOTICELIST_URL = "verify/v1/findDoctorInquiryNoticeList";
    public static final String FINDDOCTORHEALTHYCURRENCYNOTICELIST_URL = "verify/v1/findDoctorHealthyCurrencyNoticeList";
    public static final String FINDDOCTORNOTICEREADNUM_URL = "verify/v1/findDoctorNoticeReadNum";
    public static final String MODIFYALLSTATUS_URL = "verify/v1/modifyAllStatus";
    public static final String FINDMYADOPTEDCOMMENTLIST_URL = "verify/v1/findMyAdoptedCommentList";
    public static final String FINDJOBTITLELIST_URL = "v1/findJobTitleList";
    public static final String CHECKCODE_URL = "v1/checkCode";
    public static final String RESETUSERPWD_URL = "v1/resetUserPwd";
    // Sickcircle相关
    public static final String FINDSICKCIRCLELIST_URL = "sickCircle/v1/findSickCircleList";
    public static final String FINDSICKCIRCLEINFO_URL = "sickCircle/v1/findSickCircleInfo";
    public static final String SEARCHSICKCIRCLE_URL = "sickCircle/v1/searchSickCircle";
    public static final String PUBLISHCOMMENT_URL = "sickCircle/verify/v1/publishComment";
    // Inquiry相关
    public static final String FINDINQUIRYRECORDLIST_URL = "inquiry/verify/v1/findInquiryRecordList";
    public static final String FINDUSERINFO_URL = "inquiry/verify/v1/findUserInfo";
    public static final String ENDINQUIRY_URL = "inquiry/verify/v1/endInquiry";
    public static final String FINDINQUIRYDETAILSLIST_URL = "inquiry/verify/v1/findInquiryDetailsList";
    public static final String PUSHMESSAGE_URL = "inquiry/verify/v1/pushMessage";
    public static final String FINDHISTORYINQUIRYRECORD_URL = "inquiry/verify/v1/findHistoryInquiryRecord";
    public static final String FINDDOCTOREVALUATE_URL = "inquiry/verify/v1/findDoctorEvaluate";
}
