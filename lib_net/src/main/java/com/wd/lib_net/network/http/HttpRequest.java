package com.wd.lib_net.network.http;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.Part;

public interface HttpRequest {
    void post(String url, HashMap<String, Object> params, ICallback callback);

    void put(String url, HashMap<String, Object> params, ICallback callback);

    void post(String url, @Body String body, ICallback callback);

    void get(String url, HashMap<String, Object> params, ICallback callback);

    void uploadFile(String url, @Part MultipartBody.Part part, ICallback callback);
}
