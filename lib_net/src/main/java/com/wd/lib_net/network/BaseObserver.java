package com.wd.lib_net.network;


import com.wd.lib_net.NetConstants;
import com.wd.lib_net.network.rx.exception.ApiException;

import io.reactivex.Observer;

/**
 */
public abstract class BaseObserver<T> implements Observer<T> {

    public BaseObserver() {
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(Throwable e) {
        //统一处理错误
        String msg = ApiException.handlerException(e).getMsg();
        int errorCode = ApiException.handlerException(e).getErrorCode();

        if (errorCode == NetConstants.EXPIRED_TOKEN){
            //跳转至登录页面
           /* Intent intent = new Intent(NetApp.getAppContext(), LoginActivity.class);
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
            NetApp.getAppContext().startActivity(intent);*/
        }
        onError(errorCode,msg);
    }

    /**
     * 返回错误字符串
     *
     * @param msg
     */
    public abstract void onError(int errorCode,String msg);

    @Override
    public abstract void onNext(T t);

}
