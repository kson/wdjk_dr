package com.wd.lib_net.network.http;


import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.wd.lib_net.ApiService;
import com.wd.lib_net.network.BaseObserver;
import com.wd.lib_net.network.RetrofitHelper;
import com.wd.lib_net.network.rx.HttpResultFunc;
import com.wd.lib_net.network.rx.RxSchedulers;

import java.util.HashMap;
import java.util.Locale;

import io.reactivex.disposables.Disposable;
import okhttp3.MultipartBody;

public class RetrofitHttpRequest implements HttpRequest {
    private ApiService apiService;

    public RetrofitHttpRequest(boolean iscache) {
        apiService = RetrofitHelper.createService(ApiService.class, iscache);
    }

    //获取当前的系统语言
    public static String getLocalLanguage() {
        String language = "";
        String locale = Locale.getDefault().toString();
        if (locale.contains("zh")) {
            language = "zh_CN";
        } else {
            language = "en_US";
        }
        return language;
    }

    @Override
    public void post(String url, HashMap<String, Object> params, final ICallback callback) {
        HashMap<String, String> header = new HashMap<>();
        header.put("ak", "0110010010000");
        header.put("doctorId", "" + SPUtils.getInstance().getInt("doctorId"));
        header.put("sessionId", "" + SPUtils.getInstance().getString("sessionId"));
        apiService.getData(header, url, params)
                .map(new HttpResultFunc())
                .compose(RxSchedulers.<String>io_main())
                .subscribe(new BaseObserver<String>() {

                    @Override
                    public void onError(int errorCode, String msg) {
                        LogUtils.e("网络错误码：" + errorCode + "\n" + msg);

                        callback.onErrorMsg(errorCode, msg);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String dataBean) {
                        callback.onNext(dataBean);
                    }
                });
    }

    @Override
    public void put(String url, HashMap<String, Object> params, final ICallback callback) {
        HashMap<String, String> header = new HashMap<>();
        header.put("ak", "0110010010000");
        header.put("doctorId", "" + SPUtils.getInstance().getInt("doctorId"));
        header.put("sessionId", "" + SPUtils.getInstance().getString("sessionId"));
        apiService.putData(header, url, params)
                .map(new HttpResultFunc())
                .compose(RxSchedulers.<String>io_main())
                .subscribe(new BaseObserver<String>() {

                    @Override
                    public void onError(int errorCode, String msg) {
                        LogUtils.e("网络错误码：" + errorCode + "\n" + msg);
                        callback.onErrorMsg(errorCode, msg);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String dataBean) {
                        callback.onNext(dataBean);
                    }
                });
    }

    @Override
    public void post(String url, String body, final ICallback callback) {
        HashMap<String, String> header = new HashMap<>();
        header.put("ak", "0110010010000");
        header.put("doctorId", "" + SPUtils.getInstance().getInt("doctorId"));
        header.put("sessionId", "" + SPUtils.getInstance().getString("sessionId"));
        apiService.getData(url, body)
                .map(new HttpResultFunc())
                .compose(RxSchedulers.<String>io_main())
                .subscribe(new BaseObserver<String>() {

                    @Override
                    public void onError(int errorCode, String msg) {
                        LogUtils.e("网络错误码：" + errorCode + "\n" + msg);

                        callback.onErrorMsg(errorCode, msg);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String dataBean) {
                        callback.onNext(dataBean);
                    }
                });
    }

    @Override
    public void get(String url, HashMap<String, Object> params, final ICallback callback) {

        HashMap<String, String> header = new HashMap<>();
        header.put("ak", "0110010010000");
        header.put("doctorId", ""+SPUtils.getInstance().getInt("doctorId"));
        header.put("sessionId", SPUtils.getInstance().getString("sessionId"));
        apiService.doGetData(header, url, params)
                .map(new HttpResultFunc())
                .compose(RxSchedulers.<String>io_main())
                .subscribe(new BaseObserver<String>() {

                    @Override
                    public void onError(int errorCode, String msg) {
                        LogUtils.e("网络错误码：" + errorCode + "\n" + msg);

                        callback.onErrorMsg(errorCode, msg);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String dataBean) {
                        callback.onNext(dataBean);
                    }
                });
    }

    @Override
    public void uploadFile(String url, MultipartBody.Part part, final ICallback callback) {
        HashMap<String, String> header = new HashMap<>();
        header.put("ak", "0110010010000");
        header.put("doctorId", "" + SPUtils.getInstance().getInt("doctorId"));
        header.put("sessionId", "" + SPUtils.getInstance().getString("sessionId"));
        apiService.uploadFile(header, url, part)
                .map(new HttpResultFunc())
                .compose(RxSchedulers.<String>io_main())
                .subscribe(new BaseObserver<String>() {
                    @Override
                    public void onError(int errorCode, String msg) {
                        LogUtils.e("网络错误码：" + errorCode + "\n" + msg);
                        callback.onErrorMsg(errorCode, msg);
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String dataBean) {
                        callback.onNext(dataBean);
                    }
                });
    }
}
