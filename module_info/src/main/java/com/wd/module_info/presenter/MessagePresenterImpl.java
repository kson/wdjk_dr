package com.wd.module_info.presenter;

import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.contract.IMessageContract;
import com.wd.module_info.entity.MessageEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/11 16:45
 * Description：
 */
public class MessagePresenterImpl extends IMessageContract.MessagePresenter {
    @Override
    public void getMessage(HashMap<String, Object> params) {
        model.getMessage(params, new PresenterCallback<MessageEntity>() {
            @Override
            public void onSuccess(MessageEntity messageEntity) {
                baseView.showMessage(messageEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
