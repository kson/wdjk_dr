package com.wd.module_info.presenter;

import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.contract.IWalletContract;
import com.wd.module_info.entity.BalanceEntity;
import com.wd.module_info.entity.CoinEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/13 20:14
 * Description：
 */
public class WalletPresenterImpl extends IWalletContract.WalletPresenter {
    @Override
    public void getHCoin(HashMap<String, Object> params) {
        model.getHCoin(params, new PresenterCallback<CoinEntity>() {
            @Override
            public void onSuccess(CoinEntity coinEntity) {
                baseView.showCoin(coinEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }

    @Override
    public void getBalance(HashMap<String, Object> params) {
        model.getBalance(params, new PresenterCallback<BalanceEntity>() {
            @Override
            public void onSuccess(BalanceEntity balanceEntity) {
                baseView.showBalance(balanceEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
