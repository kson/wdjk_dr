package com.wd.module_info.presenter;

import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.contract.IHeadContract;
import com.wd.module_info.entity.HeadsEntity;
import com.wd.module_info.entity.UploadEntity;

import java.util.HashMap;

import okhttp3.MultipartBody;

/**
 * Author:杨帅
 * Date:2019/8/9 21:10
 * Description：
 */
public class HeadPresenterImpl extends IHeadContract.HeadPresenter {
    @Override
    public void getHeads(HashMap<String, Object> params) {
        model.getHeads(params, new PresenterCallback<HeadsEntity>() {
            @Override
            public void onSuccess(HeadsEntity headsEntity) {
                baseView.showHeads(headsEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }

    @Override
    public void selectHead(HashMap<String, Object> params) {
        model.selectHead(params, new PresenterCallback<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                baseView.showResults(baseResponse);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }

    @Override
    public void uploadHead(MultipartBody.Part part) {
        model.uploadHead(part, new PresenterCallback<UploadEntity>() {
            @Override
            public void onSuccess(UploadEntity uploadEntity) {
                baseView.showResult(uploadEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
