package com.wd.module_info.presenter;

import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.contract.IHistoryContract;
import com.wd.module_info.entity.HistoryEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/12 21:18
 * Description：
 */
public class HistoryPresenterImpl extends IHistoryContract.HistoryPresenter {
    @Override
    public void getHistory(HashMap<String, Object> params) {
        model.getHistory(params, new PresenterCallback<HistoryEntity>() {
            @Override
            public void onSuccess(HistoryEntity historyEntity) {
                baseView.showHistory(historyEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
