package com.wd.module_info.presenter;

import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.contract.ICardContract;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/14 20:00
 * Description：
 */
public class CardPresenterImpl extends ICardContract.CardPresenter {
    @Override
    public void getIdInfo(HashMap<String, Object> params) {
        model.getIdInfo(params, new PresenterCallback<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                baseView.showIdInfo(baseResponse);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }

    @Override
    public void getBankInfo(HashMap<String, Object> params) {
        model.getBankInfo(params, new PresenterCallback<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse baseResponse) {
                baseView.showBankInfo(baseResponse);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
