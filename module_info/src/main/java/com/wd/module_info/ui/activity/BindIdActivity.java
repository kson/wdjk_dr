package com.wd.module_info.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.wd.lib_core.base.BaseActivity;
import com.wd.module_info.R;

import butterknife.BindView;
import butterknife.OnClick;

public class BindIdActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.front)
    ImageView front;
    @BindView(R.id.camera1)
    ImageView camera1;
    @BindView(R.id.reverse)
    ImageView reverse;
    @BindView(R.id.camera2)
    ImageView camera2;
    @BindView(R.id.next)
    Button nextBtn;

    @Override
    protected int bindLayout() {
        return R.layout.activity_bind_id;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    protected void initData() {

    }

    @OnClick({R.id.camera1, R.id.camera2, R.id.next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.camera1:
                break;
            case R.id.camera2:
                break;
            case R.id.next:
                break;
        }
    }
}
