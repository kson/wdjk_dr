package com.wd.module_info.ui.activity;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wd.lib_core.base.BaseActivity;
import com.wd.module_info.R;
import com.wd.module_info.entity.MessageEntity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class MineActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.message)
    ImageView message;
    @BindView(R.id.head)
    ImageView head;
    @BindView(R.id.info)
    TextView info;
    @BindView(R.id.card1)
    CardView card1;
    @BindView(R.id.card2)
    CardView card2;
    @BindView(R.id.card3)
    CardView card3;
    @BindView(R.id.card4)
    CardView card4;
    @BindView(R.id.select)
    CardView cardView;
    @BindView(R.id.change)
    Button change;
    @BindView(R.id.cancel)
    Button cancel;
    private boolean flag;

    @Override
    protected int bindLayout() {
        return R.layout.activity_mine;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true)
    public void getInfo(MessageEntity messageEntity) {
        Glide.with(this).load(messageEntity.getResult().getImagePic()).into(head);
    }

    @OnClick({R.id.back, R.id.message, R.id.head, R.id.info, R.id.card1, R.id.card2, R.id.card3, R.id.card4, R.id.change, R.id.cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.message:
                break;
            case R.id.head:
                if (!flag) {
                    cardView.setVisibility(View.VISIBLE);
                    flag = true;
                } else {
                    cardView.setVisibility(View.GONE);
                    flag = false;
                }
                break;
            case R.id.info:
                startIntentLeft(PerInfoActivity.class);
                break;
            case R.id.card1:
                startIntentLeft(HistoryActivity.class);
                break;
            case R.id.card2:
                startIntentLeft(WalletActivity.class);
                break;
            case R.id.card3:
                break;
            case R.id.card4:
                break;
            case R.id.change:
                cardView.setVisibility(View.GONE);
                flag = false;
                startIntentLeft(UploadActivity.class);
                break;
            case R.id.cancel:
                cardView.setVisibility(View.GONE);
                flag = false;
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
