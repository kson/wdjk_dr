package com.wd.module_info.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_net.network.BaseResponse;
import com.wd.module_info.R;
import com.wd.module_info.contract.ICardContract;
import com.wd.module_info.model.CardModelImpl;
import com.wd.module_info.presenter.CardPresenterImpl;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class BindActivity extends BaseMvpActivity<CardModelImpl, CardPresenterImpl> implements ICardContract.ICardView {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.idcard)
    ImageView idcard;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.sex)
    TextView sex;
    @BindView(R.id.national)
    TextView national;
    @BindView(R.id.number)
    TextView number;
    @BindView(R.id.bind_id)
    TextView bindId;
    @BindView(R.id.bankcard)
    ImageView bankcard;
    @BindView(R.id.bank)
    TextView bank;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.cnumber)
    TextView cnumber;
    @BindView(R.id.bind_bank)
    TextView bindBank;
    private HashMap<String, Object> params;

    @Override
    protected int bindLayout() {
        return R.layout.activity_bind;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    public void loadData() {
        params = new HashMap<>();
        presenter.getIdInfo(params);
        presenter.getBankInfo(params);
    }

    @Override
    public void showIdInfo(BaseResponse baseResponse) {
        showToast(baseResponse.getMessage());
    }

    @Override
    public void showBankInfo(BaseResponse baseResponse) {
        showToast(baseResponse.getMessage());
    }

    @Override
    public BasePresenter initPresenter() {
        return new CardPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @OnClick({R.id.back, R.id.bind_id, R.id.bind_bank})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                break;
            case R.id.bind_id:
                startIntentLeft(BindIdActivity.class);
                break;
            case R.id.bind_bank:
                break;
        }
    }
}
