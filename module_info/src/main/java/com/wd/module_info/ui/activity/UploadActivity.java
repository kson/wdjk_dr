package com.wd.module_info.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_net.network.BaseResponse;
import com.wd.module_info.R;
import com.wd.module_info.contract.IHeadContract;
import com.wd.module_info.entity.HeadsEntity;
import com.wd.module_info.entity.UploadEntity;
import com.wd.module_info.model.HeadModelImpl;
import com.wd.module_info.presenter.HeadPresenterImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadActivity extends BaseMvpActivity<HeadModelImpl, HeadPresenterImpl> implements IHeadContract.IHeadView {

    @BindView(R.id.banner)
    BGABanner banner;
    @BindView(R.id.complete)
    Button complete;
    @BindView(R.id.camera)
    Button cameraBtn;
    @BindView(R.id.cancel)
    Button cancelBtn;
    @BindView(R.id.select)
    CardView select;
    @BindView(R.id.album)
    Button album;
    private HashMap<String, Object> params;
    private List<HeadsEntity.ResultBean> result;

    @Override
    protected int bindLayout() {
        return R.layout.activity_upload;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }


    @Override
    public void loadData() {
        params = new HashMap<>();
        presenter.getHeads(params);
    }

    @Override
    public void showHeads(HeadsEntity headsEntity) {
        result = headsEntity.getResult();
        List<View> list = new ArrayList<>();
        for (HeadsEntity.ResultBean resultBean : result) {
            ImageView imageView = new ImageView(this);
            Glide.with(this).load(resultBean.getImagePic()).into(imageView);
            list.add(imageView);
        }
        list.add(View.inflate(this, R.layout.end_image, null));
        banner.setData(list);
        banner.setDelegate(new BGABanner.Delegate() {
            @Override
            public void onBannerItemClick(BGABanner banner, View itemView, @Nullable Object model, int position) {
                if (position == result.size()) {
                    select.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void showResult(UploadEntity uploadEntity) {
        showToast(uploadEntity.getMessage());
        if (uploadEntity.getStatus().equals("0000")) {
            startIntentLeft(MainActivity.class);
            finish();
        }
    }

    @Override
    public void showResults(BaseResponse baseResponse) {
        showToast(baseResponse.getMessage());
        if (baseResponse.getStatus().equals("0000")) {
            startIntentLeft(MainActivity.class);
            finish();
        }
    }

    @Override
    public BasePresenter initPresenter() {
        return new HeadPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @OnClick({R.id.complete, R.id.camera, R.id.album, R.id.cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.complete:
                int currentItem = banner.getCurrentItem();
                if (currentItem != result.size()) {
                    HeadsEntity.ResultBean resultBean = result.get(currentItem);
                    String imagePic = resultBean.getImagePic();
                    params.put("imagePic", imagePic);
                    presenter.selectHead(params);
                }
                break;
            case R.id.camera:
                PictureSelector.create(this).openCamera(PictureMimeType.ofImage())
                        .isCamera(true)
                        .enableCrop(true)
                        .hideBottomControls(true)
                        .freeStyleCropEnabled(true)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
                select.setVisibility(View.GONE);
                break;
            case R.id.album:
                PictureSelector.create(this).openGallery(PictureMimeType.ofImage())
                        .maxSelectNum(1)
                        .selectionMode(PictureConfig.SINGLE)
                        .enableCrop(true)
                        .hideBottomControls(true)
                        .freeStyleCropEnabled(true)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
                select.setVisibility(View.GONE);
                break;
            case R.id.cancel:
                select.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PictureConfig.CHOOSE_REQUEST) {
            if (data == null) {
                return;
            }
            List<LocalMedia> localMedia = PictureSelector.obtainMultipleResult(data);
            String cutPath = localMedia.get(0).getCutPath();
            File file = new File(cutPath);
            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part imagePic = MultipartBody.Part.createFormData("imagePic", file.getName(), body);
            presenter.uploadHead(imagePic);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
