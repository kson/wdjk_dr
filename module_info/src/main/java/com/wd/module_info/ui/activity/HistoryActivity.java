package com.wd.module_info.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.widget.ImageView;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.module_info.R;
import com.wd.module_info.adapter.HistoryAdapter;
import com.wd.module_info.contract.IHistoryContract;
import com.wd.module_info.entity.HistoryEntity;
import com.wd.module_info.model.HistoryModelImpl;
import com.wd.module_info.presenter.HistoryPresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryActivity extends BaseMvpActivity<HistoryModelImpl, HistoryPresenterImpl> implements IHistoryContract.IHistoryView, XRecyclerView.LoadingListener {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.recyclerView)
    XRecyclerView recyclerView;
    private HashMap<String, Object> params;
    private List<HistoryEntity.ResultBean> results;
    private int page;
    private HistoryAdapter historyAdapter;

    @Override
    protected int bindLayout() {
        return R.layout.activity_history;
    }

    @Override
    protected void initView() {
        setImmsionBar();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLoadingMoreEnabled(true);
        recyclerView.setLoadingListener(this);
    }

    @Override
    public void loadData() {
        results = new ArrayList<>();
        params = new HashMap<>();
        page = 1;
        getData();
        historyAdapter = new HistoryAdapter(this, results);
        recyclerView.setAdapter(historyAdapter);
    }

    private void getData() {
        params.put("page", page);
        params.put("count", 5);
        presenter.getHistory(params);
    }

    @Override
    public void showHistory(HistoryEntity historyEntity) {
        List<HistoryEntity.ResultBean> result = historyEntity.getResult();
        if (page == 1) {
            results.clear();
        }
        if (result.size() > 0) {
            results.addAll(result);
        }
        historyAdapter.notifyDataSetChanged();
    }

    @Override
    public BasePresenter initPresenter() {
        return new HistoryPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }


    @OnClick(R.id.back)
    public void onViewClicked() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void onRefresh() {
        page = 1;
        getData();
        recyclerView.refreshComplete();
    }

    @Override
    public void onLoadMore() {
        page++;
        getData();
        recyclerView.loadMoreComplete();
    }
}
