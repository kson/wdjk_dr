package com.wd.module_info.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.module_info.R;
import com.wd.module_info.contract.IMessageContract;
import com.wd.module_info.entity.MessageEntity;
import com.wd.module_info.model.MessageModelImpl;
import com.wd.module_info.presenter.MessagePresenterImpl;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseMvpActivity<MessageModelImpl, MessagePresenterImpl> implements IMessageContract.IMessageView {
    @BindView(R.id.message)
    ImageView message;
    @BindView(R.id.tip)
    ImageView tip;
    @BindView(R.id.toinquiry)
    TextView toinquiry;
    @BindView(R.id.topromise)
    TextView topromise;
    @BindView(R.id.name)
    TextView nameTv;
    @BindView(R.id.hospital)
    TextView hospitalTv;
    @BindView(R.id.title)
    TextView titleTv;
    @BindView(R.id.office)
    TextView officeTv;
    @BindView(R.id.tomanager)
    TextView tomanager;
    @BindView(R.id.profile)
    ImageView profileImg;
    private HashMap<String, Object> params;
    private boolean flag;

    @Override
    protected int bindLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    public void loadData() {
        params = new HashMap<>();
        presenter.getMessage(params);
        flag = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (flag) {
            loadData();
        }
    }

    @Override
    public void showMessage(MessageEntity messageEntity) {
        if(messageEntity.getStatus().equals("0000")){
            EventBus.getDefault().postSticky(messageEntity);
            nameTv.setText(messageEntity.getResult().getName());
            hospitalTv.setText(messageEntity.getResult().getInauguralHospital());
            titleTv.setText(messageEntity.getResult().getJobTitle());
            officeTv.setText(messageEntity.getResult().getDepartmentName());
            Glide.with(this).load(messageEntity.getResult().getImagePic()).into(profileImg);
        }else{
            
        }
    }

    @Override
    public BasePresenter initPresenter() {
        return new MessagePresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @OnClick({R.id.message, R.id.toinquiry, R.id.topromise, R.id.tomanager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.message:
                break;
            case R.id.toinquiry:
                break;
            case R.id.topromise:
                break;
            case R.id.tomanager:
                startIntentLeft(MineActivity.class);
                break;
        }
    }
}