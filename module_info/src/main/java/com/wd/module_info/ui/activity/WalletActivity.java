package com.wd.module_info.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.module_info.R;
import com.wd.module_info.adapter.BalanceAdapter;
import com.wd.module_info.contract.IWalletContract;
import com.wd.module_info.entity.BalanceEntity;
import com.wd.module_info.entity.CoinEntity;
import com.wd.module_info.model.WalletModelImpl;
import com.wd.module_info.presenter.WalletPresenterImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletActivity extends BaseMvpActivity<WalletModelImpl, WalletPresenterImpl> implements IWalletContract.IWalletView, XRecyclerView.LoadingListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tobind)
    TextView tobind;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.withdrawal)
    Button withdrawal;
    @BindView(R.id.recyclerView)
    XRecyclerView recyclerView;
    private HashMap<String, Object> params;
    private List<BalanceEntity.ResultBean> results;
    private int page;
    private BalanceAdapter adapter;

    @Override
    protected int bindLayout() {
        return R.layout.activity_wallet;
    }

    @Override
    protected void initView() {
        setImmsionBar();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLoadingMoreEnabled(true);
        recyclerView.setLoadingListener(this);
    }

    @Override
    public void loadData() {
        results = new ArrayList<>();
        params = new HashMap<>();
        presenter.getHCoin(params);
        page = 1;
        getData();
        adapter = new BalanceAdapter(this, results);
        recyclerView.setAdapter(adapter);
    }

    private void getData() {
        params.put("page", page);
        params.put("count", 5);
        presenter.getBalance(params);
    }

    @Override
    public void showCoin(CoinEntity coinEntity) {
        CoinEntity.ResultBean result = coinEntity.getResult();
        count.setText(""+result.getBalance());
        if (result.getWhetherBindBankCard() == 2) {
            tobind.setText("去绑定");
        } else {
            tobind.setText("查看绑定");
        }
    }

    @Override
    public void showBalance(BalanceEntity balanceEntity) {
        List<BalanceEntity.ResultBean> result = balanceEntity.getResult();
        if (page == 1) {
            results.clear();
        }
        if (result.size() > 0) {
            results.addAll(result);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public BasePresenter initPresenter() {
        return new WalletPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @OnClick({R.id.back, R.id.tobind, R.id.withdrawal})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case R.id.tobind:
                startIntentLeft(BindActivity.class);
                break;
            case R.id.withdrawal:
                break;
        }
    }

    @Override
    public void onRefresh() {
        page = 1;
        getData();
        recyclerView.refreshComplete();
    }

    @Override
    public void onLoadMore() {
        page++;
        getData();
        recyclerView.loadMoreComplete();
    }
}
