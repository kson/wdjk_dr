package com.wd.module_info.ui.activity;

import android.widget.ImageView;
import android.widget.TextView;

import com.wd.lib_core.base.BaseActivity;
import com.wd.module_info.R;
import com.wd.module_info.entity.MessageEntity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class PerInfoActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.hospital)
    TextView hospital;
    @BindView(R.id.department)
    TextView department;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.introduce)
    TextView introduce;
    @BindView(R.id.goodat)
    TextView goodat;

    @Override
    protected int bindLayout() {
        return R.layout.activity_per_info;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Subscribe(sticky = true)
    public void getInfo(MessageEntity messageEntity) {
        name.setText(messageEntity.getResult().getName());
        hospital.setText(messageEntity.getResult().getInauguralHospital());
        department.setText(messageEntity.getResult().getDepartmentName());
        title.setText(messageEntity.getResult().getJobTitle());
        introduce.setText(messageEntity.getResult().getPersonalProfile());
        goodat.setText(messageEntity.getResult().getGoodField());
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
