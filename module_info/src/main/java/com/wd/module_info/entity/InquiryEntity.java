package com.wd.module_info.entity;

import java.util.List;

/**
 * Author:杨帅
 * Date:2019/8/12 21:06
 * Description：
 */
public class InquiryEntity {
    /**
     * result : [{"doctorHeadPic":"http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg","inquiryTime":1565612500000,"jiGuangPwd":"lfbyz1I77eExPj+OgjTPq4T7dty7z1VRFM2h9R9a5s219oMQwb90RVssA8jwOpsAo7vMjYmk2dA7ASP/L5MlRPSEyRLc/Ab+NnTvHbEamQl1vfQNPN1EaGyERaU5tUOkpg+Nvoe4dAliR+xxcQg0v9/wF5w12OiP9F6DSu4BX+M=","nickName":"2l_QKLKQ","recordId":2043,"status":1,"userHeadPic":"http://172.17.8.100/images/health/user/head_pic/default/default_head_6.jpg","userId":153,"userName":"kbTVdMchenhao1914"},{"doctorHeadPic":"http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg","inquiryTime":1565405963000,"jiGuangPwd":"R+0jdN3P4MXHPMFVe9cX5MbX5ulIXHJkfigPLKEeTBY5lUgxJWUNg0js1oGtbsKiLFL4ScqdmUbtHXIfrgQnWrwTNjf09OJLycbeJ+ka4+CV7I1eEqG8DtZPnQoCyxjoYMjO4soDl6EX9YgqaZp3DlUH4pXrYHYz58YyFkSeJEk=","nickName":"AG_DFEEF","recordId":2029,"status":1,"userHeadPic":"http://172.17.8.100/images/health/user/head_pic/default/default_head_3.jpg","userId":42,"userName":"ZDZbyE69666501"}]
     * message : 查询成功
     * status : 0000
     */

    private String message;
    private String status;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * doctorHeadPic : http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg
         * inquiryTime : 1565612500000
         * jiGuangPwd : lfbyz1I77eExPj+OgjTPq4T7dty7z1VRFM2h9R9a5s219oMQwb90RVssA8jwOpsAo7vMjYmk2dA7ASP/L5MlRPSEyRLc/Ab+NnTvHbEamQl1vfQNPN1EaGyERaU5tUOkpg+Nvoe4dAliR+xxcQg0v9/wF5w12OiP9F6DSu4BX+M=
         * nickName : 2l_QKLKQ
         * recordId : 2043
         * status : 1
         * userHeadPic : http://172.17.8.100/images/health/user/head_pic/default/default_head_6.jpg
         * userId : 153
         * userName : kbTVdMchenhao1914
         */

        private String doctorHeadPic;
        private long inquiryTime;
        private String jiGuangPwd;
        private String nickName;
        private int recordId;
        private int status;
        private String userHeadPic;
        private int userId;
        private String userName;

        public String getDoctorHeadPic() {
            return doctorHeadPic;
        }

        public void setDoctorHeadPic(String doctorHeadPic) {
            this.doctorHeadPic = doctorHeadPic;
        }

        public long getInquiryTime() {
            return inquiryTime;
        }

        public void setInquiryTime(long inquiryTime) {
            this.inquiryTime = inquiryTime;
        }

        public String getJiGuangPwd() {
            return jiGuangPwd;
        }

        public void setJiGuangPwd(String jiGuangPwd) {
            this.jiGuangPwd = jiGuangPwd;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public int getRecordId() {
            return recordId;
        }

        public void setRecordId(int recordId) {
            this.recordId = recordId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getUserHeadPic() {
            return userHeadPic;
        }

        public void setUserHeadPic(String userHeadPic) {
            this.userHeadPic = userHeadPic;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
