package com.wd.module_info.entity;

import java.util.List;

/**
 * Author:杨帅
 * Date:2019/8/12 21:23
 * Description：
 */
public class HistoryEntity {
    /**
     * result : [{"doctorHeadPic":"http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg","inquiryTime":1565271746000,"nickName":"恋人怀中樱花草","recordId":1932,"status":1,"userHeadPic":"http://172.17.8.100/images/health/user/head_pic/2019-08-08/GKJmNA20190808191817.jpeg","userId":60},{"doctorHeadPic":"http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg","inquiryTime":1565271123000,"nickName":"恋人怀中樱花草","recordId":1927,"status":1,"userHeadPic":"http://172.17.8.100/images/health/user/head_pic/2019-08-08/GKJmNA20190808191817.jpeg","userId":60},{"doctorHeadPic":"http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg","inquiryTime":1565248985000,"nickName":"AG_DFEEF","recordId":1825,"status":1,"userHeadPic":"http://172.17.8.100/images/health/user/head_pic/default/default_head_3.jpg","userId":42}]
     * message : 查询成功
     * status : 0000
     */

    private String message;
    private String status;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * doctorHeadPic : http://172.17.8.100/images/health/doctor/image_pic/default/default_image_pic.jpg
         * inquiryTime : 1565271746000
         * nickName : 恋人怀中樱花草
         * recordId : 1932
         * status : 1
         * userHeadPic : http://172.17.8.100/images/health/user/head_pic/2019-08-08/GKJmNA20190808191817.jpeg
         * userId : 60
         */

        private String doctorHeadPic;
        private long inquiryTime;
        private String nickName;
        private int recordId;
        private int status;
        private String userHeadPic;
        private int userId;

        public String getDoctorHeadPic() {
            return doctorHeadPic;
        }

        public void setDoctorHeadPic(String doctorHeadPic) {
            this.doctorHeadPic = doctorHeadPic;
        }

        public long getInquiryTime() {
            return inquiryTime;
        }

        public void setInquiryTime(long inquiryTime) {
            this.inquiryTime = inquiryTime;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public int getRecordId() {
            return recordId;
        }

        public void setRecordId(int recordId) {
            this.recordId = recordId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getUserHeadPic() {
            return userHeadPic;
        }

        public void setUserHeadPic(String userHeadPic) {
            this.userHeadPic = userHeadPic;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }
}
