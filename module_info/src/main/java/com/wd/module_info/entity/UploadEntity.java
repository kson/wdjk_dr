package com.wd.module_info.entity;

/**
 * Author:杨帅
 * Date:2019/8/9 19:42
 * Description：
 */
public class UploadEntity {

    /**
     * result : http://172.17.8.100/images/health/doctor/image_pic/2019-08-13/kD4uCD20190813160701.png
     * message : 上传成功
     * status : 0000
     */

    private String result;
    private String message;
    private String status;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
