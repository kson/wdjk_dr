package com.wd.module_info.model;

import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_info.contract.IWalletContract;
import com.wd.module_info.entity.BalanceEntity;
import com.wd.module_info.entity.CoinEntity;

import java.util.HashMap;

import cn.jmessage.support.google.gson.Gson;

/**
 * Author:杨帅
 * Date:2019/8/13 20:11
 * Description：
 */
public class WalletModelImpl implements IWalletContract.IWalletModel {
    @Override
    public void getHCoin(HashMap<String, Object> params, PresenterCallback<CoinEntity> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDDOCTORWALLET_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code, msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                CoinEntity coinEntity = gson.fromJson(t, CoinEntity.class);
                callback.onSuccess(coinEntity);
            }
        });
    }

    @Override
    public void getBalance(HashMap<String, Object> params, PresenterCallback<BalanceEntity> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDDOCTORINCOMERECORDLIST_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code, msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BalanceEntity balanceEntity = gson.fromJson(t, BalanceEntity.class);
                callback.onSuccess(balanceEntity);
            }
        });
    }
}
