package com.wd.module_info.model;

import com.google.gson.Gson;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_info.contract.IHeadContract;
import com.wd.module_info.entity.HeadsEntity;
import com.wd.module_info.entity.UploadEntity;

import java.util.HashMap;

import okhttp3.MultipartBody;

/**
 * Author:杨帅
 * Date:2019/8/9 19:40
 * Description：
 */
public class HeadModelImpl implements IHeadContract.IHeadModel {
    @Override
    public void getHeads(HashMap<String, Object> params,PresenterCallback<HeadsEntity> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDSYSTEMIMAGEPIC_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                HeadsEntity headsEntity = gson.fromJson(t, HeadsEntity.class);
                callback.onSuccess(headsEntity);
            }
        });
    }

    @Override
    public void selectHead(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback) {
        HttpRequestPresenter.getInstance().post(Api.CHOOSEIMAGEPIC_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseResponse baseResponse = gson.fromJson(t, BaseResponse.class);
                callback.onSuccess(baseResponse);
            }
        });
    }

    @Override
    public void uploadHead(MultipartBody.Part part, PresenterCallback<UploadEntity> callback) {
        HttpRequestPresenter.getInstance().uploadFile(Api.UPLOADIMAGEPIC_URL, part, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                UploadEntity uploadEntity = gson.fromJson(t, UploadEntity.class);
                callback.onSuccess(uploadEntity);
            }
        });
    }
}
