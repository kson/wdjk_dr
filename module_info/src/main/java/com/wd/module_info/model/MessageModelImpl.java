package com.wd.module_info.model;

import com.google.gson.Gson;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_info.contract.IMessageContract;
import com.wd.module_info.entity.MessageEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/11 16:44
 * Description：
 */
public class MessageModelImpl implements IMessageContract.IMessageModel {
    @Override
    public void getMessage(HashMap<String, Object> params, PresenterCallback<MessageEntity> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDDOCTORBYID_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                MessageEntity messageEntity = gson.fromJson(t, MessageEntity.class);
                callback.onSuccess(messageEntity);
            }
        });
    }
}
