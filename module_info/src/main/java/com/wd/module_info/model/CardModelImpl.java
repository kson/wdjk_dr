package com.wd.module_info.model;

import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_info.contract.ICardContract;

import java.util.HashMap;

import cn.jmessage.support.google.gson.Gson;

/**
 * Author:杨帅
 * Date:2019/8/14 19:58
 * Description：
 */
public class CardModelImpl implements ICardContract.ICardModel {
    @Override
    public void getIdInfo(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDDOCTORIDCARDINFO_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseResponse headsEntity = gson.fromJson(t, BaseResponse.class);
                callback.onSuccess(headsEntity);
            }
        });
    }

    @Override
    public void getBankInfo(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDDOCTORBANKCARDBYID_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseResponse headsEntity = gson.fromJson(t, BaseResponse.class);
                callback.onSuccess(headsEntity);
            }
        });
    }
}
