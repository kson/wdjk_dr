package com.wd.module_info.model;

import com.google.gson.Gson;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_info.contract.IHistoryContract;
import com.wd.module_info.entity.HeadsEntity;
import com.wd.module_info.entity.HistoryEntity;
import com.wd.module_info.entity.MessageEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/12 21:12
 * Description：
 */
public class HistoryModelImpl implements IHistoryContract.IHistoryModel {
    @Override
    public void getHistory(HashMap<String, Object> params, PresenterCallback<HistoryEntity> callback) {
        HttpRequestPresenter.getInstance().get(Api.FINDHISTORYINQUIRYRECORD_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                HistoryEntity historyEntity = gson.fromJson(t, HistoryEntity.class);
                callback.onSuccess(historyEntity);
            }
        });
    }
}
