package com.wd.module_info.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wd.module_info.R;
import com.wd.module_info.entity.HistoryEntity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author:杨帅
 * Date:2019/8/13 15:01
 * Description：
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ListHolder> {
    private Context context;
    private List<HistoryEntity.ResultBean> result;

    public HistoryAdapter(Context context, List<HistoryEntity.ResultBean> result) {
        this.context = context;
        this.result = result;
    }

    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ListHolder(inflater.inflate(R.layout.history_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListHolder listHolder, int i) {
        HistoryEntity.ResultBean resultBean = result.get(i);
        listHolder.drawee.setImageURI(Uri.parse(resultBean.getUserHeadPic()));
        listHolder.name.setText(resultBean.getNickName());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(resultBean.getInquiryTime());
        String format = simpleDateFormat.format(instance.getTime());
        listHolder.time.setText(format);
        if (resultBean.getStatus() == 1) {
            listHolder.appraise.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        if (result != null) {
            return result.size();
        }
        return 0;
    }

    public class ListHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.drawee)
        SimpleDraweeView drawee;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.record)
        Button record;
        @BindView(R.id.appraise)
        Button appraise;

        public ListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
