package com.wd.module_info.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wd.module_info.R;
import com.wd.module_info.entity.BalanceEntity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Author:杨帅
 * Date:2019/8/13 15:01
 * Description：
 */
public class BalanceAdapter extends RecyclerView.Adapter<BalanceAdapter.ListHolder> {

    private Context context;
    private List<BalanceEntity.ResultBean> result;

    public BalanceAdapter(Context context, List<BalanceEntity.ResultBean> result) {
        this.context = context;
        this.result = result;
    }

    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ListHolder(inflater.inflate(R.layout.wallet_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListHolder listHolder, int i) {
        BalanceEntity.ResultBean resultBean = result.get(i);
        switch (resultBean.getIncomeType()){
            case 1:
                listHolder.type.setText("问诊咨询");
                break;
            case 2:
                listHolder.type.setText("病友圈建议被采纳");
                break;
            case 3:
                listHolder.type.setText("收礼物");
                break;
            case 4:
                listHolder.type.setText("提现");
                break;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(resultBean.getRecordTime());
        String format = simpleDateFormat.format(instance.getTime());
        listHolder.time.setText(format);
        if(resultBean.getDirection()==1){
            listHolder.count.setText("+"+resultBean.getMoney()+"H币");
            listHolder.count.setTextColor(context.getResources().getColor(R.color.colorRed));
        }else{
            listHolder.count.setText("-"+resultBean.getMoney()+"H币");
            listHolder.count.setTextColor(context.getResources().getColor(R.color.colorBlue));
        }
    }

    @Override
    public int getItemCount() {
        if (result != null) {
            return result.size();
        }
        return 0;
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.count)
        TextView count;

        public ListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
