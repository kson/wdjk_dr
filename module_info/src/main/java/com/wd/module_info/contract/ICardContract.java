package com.wd.module_info.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.model.CardModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/14 19:55
 * Description：
 */
public interface ICardContract {
    interface ICardModel extends IBaseModel {
        void getIdInfo(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback);

        void getBankInfo(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback);
    }

    interface ICardView extends IBaseView {
        void showIdInfo(BaseResponse baseResponse);

        void showBankInfo(BaseResponse baseResponse);
    }

    abstract class CardPresenter extends BasePresenter<ICardModel, ICardView> {

        @Override
        public ICardModel getModel() {
            return new CardModelImpl();
        }

        protected abstract void getIdInfo(HashMap<String, Object> params);

        protected abstract void getBankInfo(HashMap<String, Object> params);

    }
}
