package com.wd.module_info.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.BaseResponse;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.entity.HeadsEntity;
import com.wd.module_info.entity.UploadEntity;
import com.wd.module_info.model.HeadModelImpl;

import java.util.HashMap;

import okhttp3.MultipartBody;

/**
 * Author:杨帅
 * Date:2019/8/9 19:37
 * Description：
 */
public interface IHeadContract {
    interface IHeadModel extends IBaseModel {
        void getHeads(HashMap<String, Object> params, PresenterCallback<HeadsEntity> callback);

        void selectHead(HashMap<String, Object> params, PresenterCallback<BaseResponse> callback);

        void uploadHead(MultipartBody.Part part, PresenterCallback<UploadEntity> callback);
    }

    interface IHeadView extends IBaseView {
        void showHeads(HeadsEntity headsEntity);

        void showResult(UploadEntity uploadEntity);

        void showResults(BaseResponse baseResponse);
    }

    abstract class HeadPresenter extends BasePresenter<IHeadModel, IHeadView> {

        @Override
        public IHeadModel getModel() {
            return new HeadModelImpl();
        }

        protected abstract void getHeads(HashMap<String, Object> params);

        protected abstract void selectHead(HashMap<String, Object> params);

        protected abstract void uploadHead(MultipartBody.Part part);

    }
}
