package com.wd.module_info.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.entity.BalanceEntity;
import com.wd.module_info.entity.CoinEntity;
import com.wd.module_info.model.WalletModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/13 20:04
 * Description：
 */
public interface IWalletContract {
    interface IWalletModel extends IBaseModel {
        void getHCoin(HashMap<String, Object> params, PresenterCallback<CoinEntity> callback);

        void getBalance(HashMap<String, Object> params, PresenterCallback<BalanceEntity> callback);
    }

    interface IWalletView extends IBaseView {
        void showCoin(CoinEntity coinEntity);

        void showBalance(BalanceEntity balanceEntity);
    }

    abstract class WalletPresenter extends BasePresenter<IWalletModel, IWalletView> {

        @Override
        public IWalletModel getModel() {
            return new WalletModelImpl();
        }

        protected abstract void getHCoin(HashMap<String, Object> params);

        protected abstract void getBalance(HashMap<String, Object> params);

    }
}
