package com.wd.module_info.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.entity.MessageEntity;
import com.wd.module_info.model.MessageModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/11 16:41
 * Description：
 */
public interface IMessageContract {
    interface IMessageModel extends IBaseModel {
        void getMessage(HashMap<String, Object> params, PresenterCallback<MessageEntity> callback);
    }

    interface IMessageView extends IBaseView {
        void showMessage(MessageEntity messageEntity);
    }

    abstract class MessagePresenter extends BasePresenter<IMessageModel, IMessageView> {

        @Override
        public IMessageModel getModel() {
            return new MessageModelImpl();
        }

        protected abstract void getMessage(HashMap<String, Object> params);
        
    }
}
