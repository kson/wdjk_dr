package com.wd.module_info.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_info.entity.HistoryEntity;
import com.wd.module_info.entity.MessageEntity;
import com.wd.module_info.model.HistoryModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/12 21:10
 * Description：
 */
public interface IHistoryContract {
    interface IHistoryModel extends IBaseModel {
        void getHistory(HashMap<String, Object> params, PresenterCallback<HistoryEntity> callback);
    }

    interface IHistoryView extends IBaseView {
        void showHistory(HistoryEntity historyEntity);
    }

    abstract class HistoryPresenter extends BasePresenter<IHistoryModel, IHistoryView> {

        @Override
        public IHistoryModel getModel() {
            return new HistoryModelImpl();
        }

        protected abstract void getHistory(HashMap<String, Object> params);

    }
}
