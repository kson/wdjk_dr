package com.wd.module_inquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wd.module_inquiry.R;
import com.wd.module_inquiry.entity.ChatMsg;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 *@Auther:陈浩
 *@Date: 2019/8/7
 *@Time:16:08
 *@Description:${DESCRIPTION}
 * */
public class ChatMsgAdpter extends RecyclerView.Adapter<ChatMsgAdpter.ChatViewHolder> {
    private Context context;
    private List<ChatMsg> chatMsgList;

    public ChatMsgAdpter(Context context, List<ChatMsg> chatMsgList) {
        this.context = context;
        this.chatMsgList = chatMsgList;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater from = LayoutInflater.from(context);
        return new ChatViewHolder(from.inflate(R.layout.chat_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int i) {
        String picHead = SPUtils.getInstance("doctor").getString("headPic");
        //设置头像
        /*Glide.with(context).load(picHead)
                .apply(new RequestOptions().circleCrop())
                .into(chatViewHolder.chatMyhead);*/

        ChatMsg chatMsg = chatMsgList.get(i);//获取当前的chatMsg实例
        //对消息的类型进行判断，显示对应的布局，隐藏另一个布局。
        if (chatMsg.getType() == ChatMsg.TYPE_RECEIVED) {
            chatViewHolder.leftLayout.setVisibility(View.VISIBLE);
            chatViewHolder.rightLayout.setVisibility(View.GONE);
            chatViewHolder.chatLeftMsg.setText(chatMsg.getContent());
        } else if (chatMsg.getType() == ChatMsg.TYPE_SEND) {
            chatViewHolder.leftLayout.setVisibility(View.GONE);
            chatViewHolder.rightLayout.setVisibility(View.VISIBLE);
            chatViewHolder.chatRightMsg.setText(chatMsg.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return chatMsgList.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.chat_youhead)
        ImageView chatYouhead;
        @BindView(R.id.chat_left_msg)
        TextView chatLeftMsg;
        @BindView(R.id.left_layout)
        LinearLayout leftLayout;
        @BindView(R.id.chat_right_msg)
        TextView chatRightMsg;
        @BindView(R.id.chat_myhead)
        ImageView chatMyhead;
        @BindView(R.id.right_layout)
        RelativeLayout rightLayout;

        //传入view参数，获取布局中的控件
        public ChatViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }
    }
}
