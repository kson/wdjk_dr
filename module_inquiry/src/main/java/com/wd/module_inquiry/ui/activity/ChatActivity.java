package com.wd.module_inquiry.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.wd.lib_core.base.BaseActivity;
import com.wd.lib_core.utils.MD5Utils;
import com.wd.module_inquiry.R;
import com.wd.module_inquiry.adapter.ChatMsgAdpter;
import com.wd.module_inquiry.entity.ChatMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.event.MessageEvent;
import cn.jpush.im.android.api.event.OfflineMessageEvent;
import cn.jpush.im.android.api.model.Message;
import cn.jpush.im.api.BasicCallback;

public class ChatActivity extends BaseActivity {

    @BindView(R.id.chat_back)
    ImageView chatBack;
    @BindView(R.id.doctor_name)
    TextView doctorName;
    @BindView(R.id.chat_xrecy)
    XRecyclerView chatXrecy;
    @BindView(R.id.chat_type)
    ImageView chatSendType;
    @BindView(R.id.chat_edit)
    EditText chatXrecyEdt;
    @BindView(R.id.chat_video)
    Button chatVideo;
    @BindView(R.id.chat_send)
    Button chatSend;
    @BindView(R.id.chat_emjo)
    ImageView chatSendEmjo;
    @BindView(R.id.chat_pic)
    ImageView chatSendPic;
    @BindView(R.id.Container)
    FrameLayout Container;
    private List<ChatMsg> list;
    private ChatMsgAdpter adapter;

    @Override
    protected int bindLayout() {
        return R.layout.activity_chat;
    }

    @Override
    protected void initView() {
        setImmsionBar();
        chatXrecy.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    protected void initData() {
        list = new ArrayList<>();
        JMessageClient.registerEventReceiver(this);
        JMessageClient.login("88ibPh1152722231", MD5Utils.MD5("123456"), new BasicCallback() {
            @Override
            public void gotResult(int i, String s) {
                showToast(s);
            }
        });
        adapter = new ChatMsgAdpter(this, list);
        chatXrecy.setAdapter(adapter);
    }

    @OnClick({R.id.chat_back, R.id.chat_type, R.id.chat_edit, R.id.chat_video, R.id.chat_send, R.id.chat_emjo, R.id.chat_pic, R.id.Container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chat_back:
                break;
            case R.id.chat_type:
                break;
            case R.id.chat_edit:
                break;
            case R.id.chat_video:
                break;
            case R.id.chat_send:
                String content = chatXrecyEdt.getText().toString().trim();
                if (!TextUtils.isEmpty(content)) {
                    Message message = JMessageClient.createSingleTextMessage("bbbb", "c7f6a1d56cb8da740fd18bfa", content);
                    //Message message = JMessageClient.createSingleTextMessage("2333", "b5f102cc307091e167ce52e0", content);
                    JMessageClient.sendMessage(message);
                    ChatMsg chatMsg = new ChatMsg(content, ChatMsg.TYPE_SEND);
                    list.add(chatMsg);
                    adapter.notifyDataSetChanged();
                    chatXrecy.scrollToPosition(list.size());//滑动到最新消息位置
                    chatXrecyEdt.setText(null);
                }
                break;
            case R.id.chat_emjo:
                break;
            case R.id.chat_pic:
                break;
            case R.id.Container:
                break;
        }
    }

    public void onEventMainThread(MessageEvent event) {
        Message newMessage = event.getMessage();//获取此次离线期间会话收到的新消息列表
        String s = newMessage.getContent().toJson();
        try {
            JSONObject object = new JSONObject(s);
            String text = object.getString("text");
            ChatMsg chatMsg = new ChatMsg("" + text, ChatMsg.TYPE_RECEIVED);
            list.add(chatMsg);//添加到会话
            adapter.notifyDataSetChanged();//刷新
            chatXrecy.scrollToPosition(list.size());//滑动到最新消息位置
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
