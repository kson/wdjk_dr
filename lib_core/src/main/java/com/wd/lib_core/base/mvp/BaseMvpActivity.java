package com.wd.lib_core.base.mvp;

import com.wd.lib_core.base.BaseActivity;

/**
 * Author:杨帅
 * Date:2019/7/9 20:08
 * Description：
 */
public abstract class BaseMvpActivity<M extends IBaseModel, P extends BasePresenter> extends BaseActivity implements IBaseView {

    public P presenter;
    public M model;

    @Override
    protected void initData() {
        presenter = (P) initPresenter();
        if (presenter != null) {
            model = (M) presenter.getModel();
            if (model != null) {
                presenter.attach(this);
            }
        }
        loadData();
    }

    public abstract void loadData();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
