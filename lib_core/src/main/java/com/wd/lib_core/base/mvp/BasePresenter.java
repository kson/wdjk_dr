package com.wd.lib_core.base.mvp;

import java.lang.ref.WeakReference;

/**
 * Author:杨帅
 * Date:2019/7/9 19:58
 * Description：
 */
public abstract class BasePresenter<M, V> {
    public M model;
    public V baseView;
    private WeakReference<M> mWeakReference;
    private WeakReference<V> vWeakReference;

    public abstract M getModel();

    public void attach(V baseView) {
        this.baseView = baseView;
        vWeakReference = new WeakReference<>(baseView);
        model = getModel();
        mWeakReference = new WeakReference<>(model);

    }

    public void detach() {
        if (vWeakReference != null) {
            vWeakReference.clear();
            vWeakReference = null;
            baseView = null;
        }
        if (mWeakReference != null) {
            mWeakReference.clear();
            mWeakReference = null;
            model = null;
        }
    }
}
