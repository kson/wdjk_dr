package com.wd.lib_core.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.gyf.barlibrary.ImmersionBar;
import com.wd.lib_core.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Author:杨帅
 * Date:2019/7/8 19:48
 * Description：
 */
public abstract class BaseActivity extends AppCompatActivity {

    private Unbinder bind;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(bindLayout());
        bind = ButterKnife.bind(this);
        initView();
        initData();
    }

    /**
     * 绑定布局
     *
     * @return
     */
    protected abstract int bindLayout();

    /**
     * 初始化控件
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 显示吐司
     *
     * @param content
     */
    protected void showToast(String content) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
    }

    /**
     * 无参跳转
     *
     * @param clz
     */
    protected void startIntent(Class<?> clz) {
        startActivity(new Intent(this, clz));
    }
    /**
     * 无参跳转(自左向右)
     *
     * @param clz
     */
    protected void startIntentLeft(Class<?> clz) {
        startActivity(new Intent(this, clz));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * 有参跳转
     *
     * @param bundle
     * @param clz
     */
    protected void startIntentWithData(Bundle bundle, Class<?> clz) {
        Intent intent = new Intent(this, clz);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * 设置沉浸式状态栏
     */
    public void setImmsionBar() {
        ImmersionBar.with(this).transparentStatusBar().init();
    }

    /**
     * 设置沉浸式状态栏(自定义颜色)
     */
    public void setImmsionBarWithColor(int color) {
        ImmersionBar.with(this).transparentStatusBar().statusBarColor(color).init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null) {
            bind.unbind();
            bind = null;
        }
        ImmersionBar.with(this).destroy();
    }
}
