package com.wd.lib_core.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Author:杨帅
 * Date:2019/7/8 20:04
 * Description：
 */
public abstract class BaseFragment extends Fragment {

    private Unbinder bind;
    private boolean isUIVisible;

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            isUIVisible = true;
            lazyLoad();
        } else {
            isUIVisible = false;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            isUIVisible = true;
            lazyLoad();
        } else {
            isUIVisible = false;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(bindLayout(), container, false);
        bind = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        lazyLoad();
    }



    /**
     * 绑定布局
     *
     * @return
     */
    protected abstract int bindLayout();

    /**
     * 初始化控件
     */
    protected abstract void initView();

    /**
     * 懒加载数据
     */
    protected void lazyLoad() {
        if (isUIVisible) {
            loadData();
            isUIVisible = false;
        }
    }

    /**
     * 加载数据
     */
    protected abstract void loadData();

    /**
     * 显示Toast
     *
     * @param content
     */
    public void showToast(String content) {
        Toast.makeText(getContext(), content, Toast.LENGTH_SHORT).show();
    }

    /**
     * 无参跳转
     *
     * @param clz
     */
    protected void startIntent(Class<?> clz) {
        startActivity(new Intent(getContext(), clz));
    }

    /**
     * 有参跳转
     *
     * @param clz
     */
    protected void startIntentWithData(Bundle bundle, Class<?> clz) {
        Intent intent = new Intent(getContext(), clz);
        intent.putExtra("things", bundle);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bind != null) {
            bind.unbind();
            bind = null;
        }
    }
}
