package com.wd.lib_core.base.mvp;

import com.wd.lib_core.base.BaseFragment;

/**
 * Author:杨帅
 * Date:2019/7/9 20:08
 * Description：BaseMvpFragment基类
 */
public abstract class BaseMvpFragment<M extends IBaseModel, P extends BasePresenter> extends BaseFragment implements IBaseView {

    public P presenter;
    public M model;

    @Override
    protected void loadData() {
        presenter = (P) initPresenter();
        if (presenter != null) {
            model = (M) presenter.getModel();
            if (model != null) {
                presenter.attach(this);
            }
        }
        initData();
    }

    protected abstract void initData();

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
