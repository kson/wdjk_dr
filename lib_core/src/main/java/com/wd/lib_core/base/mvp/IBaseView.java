package com.wd.lib_core.base.mvp;

/**
 * Author:杨帅
 * Date:2019/7/9 19:58
 * Description：
 */
public interface IBaseView {
    BasePresenter initPresenter();

    void onError(String msg);
}
