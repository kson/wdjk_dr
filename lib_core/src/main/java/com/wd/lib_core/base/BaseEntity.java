package com.wd.lib_core.base;

/**
 * Author:杨帅
 * Date:2019/7/9 20:40
 * Description：
 */
public class BaseEntity<T> {
    public String message;
    public String status;
    public T result;
}
