package com.wd.lib_core.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.tencent.bugly.crashreport.CrashReport;
import com.wd.lib_net.NetApp;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.RetrofitHttpRequest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import cn.jpush.im.android.api.JMessageClient;

/**
 * Author:杨帅
 * Date:2019/7/9 19:19
 * Description：App类
 */
public class App extends Application {

    public static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        //网络层的初始化
        JMessageClient.init(this);
        NetApp.init(this, Api.BASE_URL);
        Fresco.initialize(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                //网络请求框架的初始化
                HttpRequestPresenter.init(new RetrofitHttpRequest(true));
                // 获取当前包名
                String packageName = context.getPackageName();
                // 获取当前进程名
                String processName = getProcessName(android.os.Process.myPid());
                // 设置是否为上报进程
                CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
                strategy.setUploadProcess(processName == null || processName.equals(packageName));
                // 初始化Bugly
                CrashReport.initCrashReport(context, "a975ad95c8", true, strategy);
                //CrashReport.initCrashReport(context, "a975ad95c8", true);
            }
        }).start();
    }

    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }
}
