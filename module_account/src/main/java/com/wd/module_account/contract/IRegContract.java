package com.wd.module_account.contract;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.model.RegModelImpl;

/**
 * Author:杨帅
 * Date:2019/8/6 9:54
 * Description：
 */
public interface IRegContract {
    interface IRegModel extends IBaseModel {
        void register(String body, PresenterCallback<BaseEntity> callback);
    }

    interface IRegView extends IBaseView {
        void showResult(BaseEntity baseEntity);
    }

    abstract class RegPresenter extends BasePresenter<IRegModel, IRegView> {

        @Override
        public IRegModel getModel() {
            return new RegModelImpl();
        }
        protected abstract void register(String body);
    }
}
