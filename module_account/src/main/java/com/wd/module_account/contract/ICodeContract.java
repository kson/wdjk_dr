package com.wd.module_account.contract;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_account.model.CodeModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:32
 * Description：
 */
public interface ICodeContract {
    interface ICodeModel extends IBaseModel {
        void getCode(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback);

        void checkCode(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback);
    }

    interface ICodeView extends IBaseView {
        void showResult(BaseEntity baseEntity);
    }

    abstract class CodePresenter extends BasePresenter<ICodeModel, ICodeView> {

        @Override
        public ICodeModel getModel() {
            return new CodeModelImpl();
        }

        protected abstract void getCode(HashMap<String, Object> params);

        protected abstract void checkCode(HashMap<String, Object> params);
    }
}
