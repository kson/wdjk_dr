package com.wd.module_account.contract;

import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.entity.LoginEntity;
import com.wd.module_account.model.LoginModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/8 22:34
 * Description：
 */
public interface ILoginContract {
    interface ILoginModel extends IBaseModel {
        void login(HashMap<String, Object> params, PresenterCallback<LoginEntity> callback);
    }

    interface ILoginView extends IBaseView {
        void showResult(LoginEntity loginEntity);
    }

    abstract class LoginPresenter extends BasePresenter<ILoginModel, ILoginView> {

        @Override
        public ILoginModel getModel() {
            return new LoginModelImpl();
        }

        protected abstract void login(HashMap<String, Object> params);
    }
}
