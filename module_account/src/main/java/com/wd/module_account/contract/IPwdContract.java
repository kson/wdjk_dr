package com.wd.module_account.contract;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.base.mvp.IBaseModel;
import com.wd.lib_core.base.mvp.IBaseView;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.model.PwdModelImpl;
import com.wd.module_account.model.RegModelImpl;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:54
 * Description：
 */
public interface IPwdContract {
    interface IPwdModel extends IBaseModel {
        void resetPwd(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback);
    }

    interface IPwdView extends IBaseView {
        void showResult(BaseEntity baseEntity);
    }

    abstract class PwdPresenter extends BasePresenter<IPwdModel, IPwdView> {

        @Override
        public IPwdModel getModel() {
            return new PwdModelImpl();
        }

        public abstract void resetPwd(HashMap<String, Object> params);
    }
}
