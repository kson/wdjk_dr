package com.wd.module_account.model;

import com.google.gson.Gson;
import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequest;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_account.contract.ICodeContract;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:35
 * Description：
 */
public class CodeModelImpl implements ICodeContract.ICodeModel {

    @Override
    public void getCode(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback) {
        HttpRequestPresenter.getInstance().post(Api.SENDEMAILCODE_URL,params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseEntity baseEntity = gson.fromJson(t, BaseEntity.class);
                callback.onSuccess(baseEntity);
            }
        });
    }
    @Override
    public void checkCode(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback) {
        HttpRequestPresenter.getInstance().post(Api.CHECKCODE_URL,params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code,msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseEntity baseEntity = gson.fromJson(t, BaseEntity.class);
                callback.onSuccess(baseEntity);
            }
        });
    }
}
