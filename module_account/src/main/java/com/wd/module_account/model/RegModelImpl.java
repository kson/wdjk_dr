package com.wd.module_account.model;

import com.google.gson.Gson;
import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_account.contract.IRegContract;

/**
 * Author:杨帅
 * Date:2019/8/6 9:57
 * Description：
 */
public class RegModelImpl implements IRegContract.IRegModel {

    @Override
    public void register(String body, PresenterCallback<BaseEntity> callback) {
        HttpRequestPresenter.getInstance().post(Api.APPLYJOIN_URL, body, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code, msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseEntity baseEntity = gson.fromJson(t, BaseEntity.class);
                callback.onSuccess(baseEntity);
            }
        });
    }
}
