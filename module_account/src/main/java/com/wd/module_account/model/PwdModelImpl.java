package com.wd.module_account.model;

import com.google.gson.Gson;
import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_account.contract.IPwdContract;
import com.wd.module_account.entity.LoginEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/9 18:09
 * Description：
 */
public class PwdModelImpl implements IPwdContract.IPwdModel {
    @Override
    public void resetPwd(HashMap<String, Object> params, PresenterCallback<BaseEntity> callback) {
        HttpRequestPresenter.getInstance().put(Api.RESETUSERPWD_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code, msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                BaseEntity baseEntity = gson.fromJson(t, BaseEntity.class);
                callback.onSuccess(baseEntity);
            }
        });
    }
}
