package com.wd.module_account.model;

import com.google.gson.Gson;
import com.wd.lib_net.api.Api;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.lib_net.network.http.HttpRequestPresenter;
import com.wd.lib_net.network.http.ICallback;
import com.wd.module_account.contract.ILoginContract;
import com.wd.module_account.entity.LoginEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:57
 * Description：
 */
public class LoginModelImpl implements ILoginContract.ILoginModel {

    @Override
    public void login(HashMap<String, Object> params, PresenterCallback<LoginEntity> callback) {
        HttpRequestPresenter.getInstance().post(Api.LOGIN_URL, params, new ICallback() {
            @Override
            public void onErrorMsg(int code, String msg) {
                callback.onErrorMsg(code, msg);
            }

            @Override
            public void onNext(String t) {
                Gson gson = new Gson();
                LoginEntity loginEntity = gson.fromJson(t, LoginEntity.class);
                callback.onSuccess(loginEntity);
            }
        });
    }
}
