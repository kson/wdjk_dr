package com.wd.module_account.presenter;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.contract.ICodeContract;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:38
 * Description：
 */
public class CodePresenterImpl extends ICodeContract.CodePresenter {

    @Override
    public void getCode(HashMap<String, Object> params) {
        model.getCode(params, new PresenterCallback<BaseEntity>() {

            @Override
            public void onSuccess(BaseEntity baseEntity) {
                baseView.showResult(baseEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
    @Override
    public void checkCode(HashMap<String, Object> params) {
        model.checkCode(params, new PresenterCallback<BaseEntity>() {

            @Override
            public void onSuccess(BaseEntity baseEntity) {
                baseView.showResult(baseEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
