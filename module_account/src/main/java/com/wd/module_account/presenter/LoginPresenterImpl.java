package com.wd.module_account.presenter;

import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.contract.ILoginContract;
import com.wd.module_account.entity.LoginEntity;

import java.util.HashMap;

/**
 * Author:杨帅
 * Date:2019/8/6 9:58
 * Description：
 */
public class LoginPresenterImpl extends ILoginContract.LoginPresenter {

    public void login(HashMap<String, Object> params) {
        model.login(params, new PresenterCallback<LoginEntity>() {
            @Override
            public void onSuccess(LoginEntity loginEntity) {
                baseView.showResult(loginEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
