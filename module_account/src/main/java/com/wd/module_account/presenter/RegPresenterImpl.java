package com.wd.module_account.presenter;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_net.network.PresenterCallback;
import com.wd.module_account.contract.IRegContract;

/**
 * Author:杨帅
 * Date:2019/8/6 9:58
 * Description：
 */
public class RegPresenterImpl extends IRegContract.RegPresenter {

    public void register(String body) {
        model.register(body, new PresenterCallback<BaseEntity>() {
            @Override
            public void onSuccess(BaseEntity baseEntity) {
                baseView.showResult(baseEntity);
            }

            @Override
            public void onSuccessMsg(String status, String message) {

            }

            @Override
            public void onErrorMsg(int code, String msg) {
                baseView.onError(msg);
            }
        });
    }
}
