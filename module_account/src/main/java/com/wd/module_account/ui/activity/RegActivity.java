package com.wd.module_account.ui.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.RegexUtils;
import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.utils.RsaCoder;
import com.wd.module_account.entity.RegEntity;
import com.wd.module_account.R;
import com.wd.module_account.contract.ICodeContract;
import com.wd.module_account.model.CodeModelImpl;
import com.wd.module_account.presenter.CodePresenterImpl;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class RegActivity extends BaseMvpActivity<CodeModelImpl, CodePresenterImpl> implements ICodeContract.ICodeView {

    @BindView(R.id.email)
    EditText emailEt;
    @BindView(R.id.getcode)
    TextView getcode;
    @BindView(R.id.code)
    EditText codeEt;
    @BindView(R.id.pass)
    EditText passEt;
    @BindView(R.id.cpass)
    EditText cpassEt;
    @BindView(R.id.next)
    Button nextBtn;
    private RegEntity regEntity;
    private HashMap<String, Object> params;

    @Override
    protected int bindLayout() {
        return R.layout.activity_reg;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    public void loadData() {
        regEntity = new RegEntity();
        params = new HashMap<>();
    }

    @OnClick({R.id.getcode, R.id.next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.getcode:
                String emails = emailEt.getText().toString().trim();
                if (!TextUtils.isEmpty(emails)) {
                    params.put("email", emails);
                    presenter.getCode(params);
                }
                break;
            case R.id.next:
                String email = emailEt.getText().toString().trim();
                String code = codeEt.getText().toString().trim();
                String pwd1 = passEt.getText().toString().trim();
                String pwd2 = cpassEt.getText().toString().trim();
                try {
                    if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(code) && !TextUtils.isEmpty(pwd1) && !TextUtils.isEmpty(pwd2)) {
                        if (RegexUtils.isEmail(email)) {
                            regEntity.setEmail(email);
                            regEntity.setCode(code);
                            if (pwd1.equals(pwd2)) {
                                String s = RsaCoder.encryptByPublicKey(pwd1);
                                regEntity.setPwd1(s);
                                regEntity.setPwd2(s);
                                EventBus.getDefault().postSticky(regEntity);
                                startIntentLeft(InfoActivity.class);
                            } else {
                                showToast("两次输入的密码不一致，请重新输入");
                            }
                        } else {
                            showToast("邮箱格式不对，请重新输入");
                        }
                    } else {
                        showToast("本页数据不能为空");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public BasePresenter initPresenter() {
        return new CodePresenterImpl();
    }

    @Override
    public void showResult(BaseEntity baseEntity) {
        showToast(baseEntity.message);
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
