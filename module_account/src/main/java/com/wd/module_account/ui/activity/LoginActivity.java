package com.wd.module_account.ui.activity;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.SPUtils;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.utils.RsaCoder;
import com.wd.module_account.R;
import com.wd.module_account.contract.ILoginContract;
import com.wd.module_account.entity.LoginEntity;
import com.wd.module_account.model.LoginModelImpl;
import com.wd.module_account.presenter.LoginPresenterImpl;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.api.BasicCallback;

public class LoginActivity extends BaseMvpActivity<LoginModelImpl, LoginPresenterImpl> implements ILoginContract.ILoginView {

    @BindView(R.id.email)
    EditText emailEt;
    @BindView(R.id.pass)
    EditText passEt;
    @BindView(R.id.show)
    ImageView showImg;
    @BindView(R.id.forget)
    TextView forget;
    @BindView(R.id.enter)
    TextView enter;
    @BindView(R.id.login)
    Button login;
    private HashMap<String, Object> params;

    @Override
    protected int bindLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    public void loadData() {
        params = new HashMap<>();
    }


    @OnClick({R.id.show, R.id.forget, R.id.enter, R.id.login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.show:
                if (passEt.getInputType() == 129) {
                    passEt.setInputType(128);
                    showImg.setImageResource(R.mipmap.login_icon_show_password);
                } else {
                    passEt.setInputType(129);
                    showImg.setImageResource(R.mipmap.login_icon_hide_password_n);
                }
                break;
            case R.id.forget:
                startIntentLeft(ForgetActivity.class);
                break;
            case R.id.enter:
                startIntentLeft(RegActivity.class);
                break;
            case R.id.login:
                String email = emailEt.getText().toString().trim();
                String pass = passEt.getText().toString().trim();
                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)) {
                    try {
                        params.put("email", email);
                        params.put("pwd", RsaCoder.encryptByPublicKey(pass));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    presenter.login(params);
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        overridePendingTransition(com.wd.lib_core.R.anim.slide_in_scale, com.wd.lib_core.R.anim.slide_out_scale);
    }

    @Override
    public BasePresenter initPresenter() {
        return new LoginPresenterImpl();
    }

    @Override
    public void showResult(LoginEntity loginEntity) {
        showToast(loginEntity.getMessage());
        if (loginEntity.getStatus().equals("0000")) {
            SPUtils.getInstance().put("doctorId", loginEntity.getResult().getId());
            SPUtils.getInstance().put("sessionId", loginEntity.getResult().getSessionId());
            SPUtils.getInstance().put("userName", loginEntity.getResult().getUserName());
            try {
                String s = RsaCoder.decryptByPublicKey(loginEntity.getResult().getJiGuangPwd());
                String string = EncryptUtils.encryptMD5ToString(s);
                SPUtils.getInstance().put("jiGuangPwd", string);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
