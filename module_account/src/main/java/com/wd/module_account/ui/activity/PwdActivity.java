package com.wd.module_account.ui.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.lib_core.utils.RsaCoder;
import com.wd.module_account.R;
import com.wd.module_account.contract.IPwdContract;
import com.wd.module_account.model.PwdModelImpl;
import com.wd.module_account.presenter.PwdPresenterImpl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class PwdActivity extends BaseMvpActivity<PwdModelImpl, PwdPresenterImpl> implements IPwdContract.IPwdView {

    @BindView(R.id.left_back)
    ImageView leftBack;
    @BindView(R.id.pass)
    EditText passEt;
    @BindView(R.id.see)
    ImageView see;
    @BindView(R.id.cpass)
    EditText cpassEt;
    @BindView(R.id.csee)
    ImageView csee;
    @BindView(R.id.complete)
    Button complete;
    private HashMap<String, Object> params;

    @Override
    protected int bindLayout() {
        return R.layout.activity_pwd;
    }

    @Override
    protected void initView() {
        setImmsionBar();
        params = new HashMap<>();
    }

    @Subscribe(sticky = true)
    public void getInfo(String email) {
        params.put("email", email);
    }

    @Override
    public void loadData() {
        EventBus.getDefault().register(this);
    }

    @OnClick({R.id.left_back, R.id.see, R.id.csee, R.id.complete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_back:
                finish();
                overridePendingTransition(com.wd.lib_core.R.anim.slide_in_left, com.wd.lib_core.R.anim.slide_out_right);
                break;
            case R.id.see:
                if (passEt.getInputType() == 129) {
                    passEt.setInputType(128);
                    see.setImageResource(R.mipmap.login_icon_show_password);
                } else {
                    passEt.setInputType(129);
                    see.setImageResource(R.mipmap.login_icon_hide_password_n);
                }
                break;
            case R.id.csee:
                if (cpassEt.getInputType() == 129) {
                    cpassEt.setInputType(128);
                    csee.setImageResource(R.mipmap.login_icon_show_password);
                } else {
                    cpassEt.setInputType(129);
                    csee.setImageResource(R.mipmap.login_icon_hide_password_n);
                }
                break;
            case R.id.complete:
                String pass = passEt.getText().toString().trim();
                String cpass = cpassEt.getText().toString().trim();
                if (!TextUtils.isEmpty(pass) && !TextUtils.isEmpty(cpass)) {
                    if (pass.equals(cpass)) {
                        try {
                            String s = RsaCoder.encryptByPublicKey(pass);
                            params.put("pwd1", s);
                            params.put("pwd2", s);
                            presenter.resetPwd(params);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        showToast("两次输入的密码不一致，请重新输入");
                    }
                } else {
                    showToast("密码不能为空");
                }
                break;
        }
    }


    @Override
    public void showResult(BaseEntity baseEntity) {
        showToast(baseEntity.message);
        if (baseEntity.status.equals("0000")) {
            startIntentLeft(LoginActivity.class);
        }
    }

    @Override
    public BasePresenter initPresenter() {
        return new PwdPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        presenter.detach();
    }
}
