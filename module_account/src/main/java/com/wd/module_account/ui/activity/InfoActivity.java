package com.wd.module_account.ui.activity;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.wd.lib_core.base.BaseActivity;
import com.wd.module_account.entity.RegEntity;
import com.wd.module_account.R;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class InfoActivity extends BaseActivity {

    @BindView(R.id.name)
    EditText nameEt;
    @BindView(R.id.hospital)
    EditText hospitalEt;
    @BindView(R.id.department)
    Spinner departments;
    @BindView(R.id.title)
    Spinner title;
    @BindView(R.id.next)
    Button next;
    private RegEntity regEntity;

    @Override
    protected int bindLayout() {
        return R.layout.activity_info;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true)
    public void getInfo(RegEntity regEntity) {
        this.regEntity = regEntity;
    }

    @OnClick(R.id.next)
    public void onViewClicked() {
        String name = nameEt.getText().toString().trim();
        String hospital = hospitalEt.getText().toString().trim();
        String department = departments.getSelectedItem().toString();
        String jobTitle = title.getSelectedItem().toString();
        if(!TextUtils.isEmpty(name)&!TextUtils.isEmpty(hospital)){
            regEntity.setName(name);
            regEntity.setInauguralHospital(hospital);
            regEntity.setDepartmentName(department);
            regEntity.setJobTitle(jobTitle);
            EventBus.getDefault().postSticky(regEntity);
            startIntentLeft(DetailsActivity.class);
        }else{
            showToast("姓名或医院不能为空");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
