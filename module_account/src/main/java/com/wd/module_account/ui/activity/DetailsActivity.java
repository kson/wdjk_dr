package com.wd.module_account.ui.activity;

import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.module_account.R;
import com.wd.module_account.contract.IRegContract;
import com.wd.module_account.entity.RegEntity;
import com.wd.module_account.model.RegModelImpl;
import com.wd.module_account.presenter.RegPresenterImpl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


import butterknife.BindView;
import butterknife.OnClick;

public class DetailsActivity extends BaseMvpActivity<RegModelImpl, RegPresenterImpl> implements IRegContract.IRegView {

    @BindView(R.id.introduce)
    EditText introduceEt;
    @BindView(R.id.well)
    EditText wellEt;
    @BindView(R.id.next)
    Button next;
    private RegEntity regEntity;

    @Override
    protected int bindLayout() {
        return R.layout.activity_details;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }


    @Override
    public void loadData() {
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true)
    public void getInfo(RegEntity regEntity) {
        this.regEntity = regEntity;
    }

    @OnClick(R.id.next)
    public void onViewClicked() {
        String introduce = introduceEt.getText().toString().trim();
        String well = wellEt.getText().toString().trim();
        if (!TextUtils.isEmpty(introduce) & !TextUtils.isEmpty(well)) {
            regEntity.setPersonalProfile(introduce);
            regEntity.setGoodField(well);
            Gson gson = new Gson();
            String s = gson.toJson(regEntity);
            presenter.register(s);
            startIntentLeft(WaitActivity.class);
        } else {
            showToast("个人简介或擅长领域不能为空");
        }

    }

    @Override
    public void showResult(BaseEntity baseEntity) {
        showToast(baseEntity.message);
    }

    @Override
    public BasePresenter initPresenter() {
        return new RegPresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        presenter.detach();
    }
}
