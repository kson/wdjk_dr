package com.wd.module_account.ui.activity;

import android.widget.Button;

import com.wd.lib_core.base.BaseActivity;
import com.wd.module_account.R;

import butterknife.BindView;
import butterknife.OnClick;

public class WaitActivity extends BaseActivity {

    @BindView(R.id.next)
    Button next;

    @Override
    protected int bindLayout() {
        return R.layout.activity_wait;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    protected void initData() {

    }

    @OnClick(R.id.next)
    public void onViewClicked() {
        startIntentLeft(LoginActivity.class);
    }
}
