package com.wd.module_account.ui.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.wd.lib_core.base.BaseEntity;
import com.wd.lib_core.base.mvp.BaseMvpActivity;
import com.wd.lib_core.base.mvp.BasePresenter;
import com.wd.module_account.R;
import com.wd.module_account.contract.ICodeContract;
import com.wd.module_account.model.CodeModelImpl;
import com.wd.module_account.presenter.CodePresenterImpl;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgetActivity extends BaseMvpActivity<CodeModelImpl, CodePresenterImpl> implements ICodeContract.ICodeView {

    @BindView(R.id.left_back)
    ImageView leftBack;
    @BindView(R.id.email)
    EditText emailEt;
    @BindView(R.id.getcode)
    Button getcode;
    @BindView(R.id.code)
    EditText codeEt;
    @BindView(R.id.next)
    Button nextBtn;
    private HashMap<String, Object> params;

    @Override
    protected int bindLayout() {
        return R.layout.activity_forget;
    }

    @Override
    protected void initView() {
        setImmsionBar();
    }

    @Override
    public void loadData() {
        params = new HashMap<>();
    }

    @OnClick({R.id.left_back, R.id.getcode, R.id.next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.left_back:
                finish();
                overridePendingTransition(com.wd.lib_core.R.anim.slide_in_left, com.wd.lib_core.R.anim.slide_out_right);
                break;
            case R.id.getcode:
                String emails = emailEt.getText().toString().trim();
                if (!TextUtils.isEmpty(emails)) {
                    params.put("email", emails);
                    presenter.getCode(params);
                } else {
                    showToast("邮箱不能为空");
                }
                break;
            case R.id.next:
                String email = emailEt.getText().toString().trim();
                EventBus.getDefault().postSticky(email);
                String code = codeEt.getText().toString().trim();
                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(code)) {
                    params.put("email", email);
                    params.put("code", code);
                    presenter.checkCode(params);
                } else {
                    showToast("邮箱及验证码不能为空");
                }
                break;
        }
    }

    @Override
    public void showResult(BaseEntity baseEntity) {
        showToast(baseEntity.message);
        if (baseEntity.message.equals("验证通过")) {
            startIntentLeft(PwdActivity.class);
        }
    }

    @Override
    public BasePresenter initPresenter() {
        return new CodePresenterImpl();
    }

    @Override
    public void onError(String msg) {
        showToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
